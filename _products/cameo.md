---
title: Silhouette Cameo 4
subtitle: Decoupeuse vinyle numérique
description: Decoupeuse vinyle numérique
hero_image: /campus-amiens/hub-amiens/img/machines/cameo.jpg
hero_darken: true
layout: product
image: /campus-amiens/hub-amiens/img/machines/cameo-4-3.jpg
price: 
features:
    - label: Simple et plug-n-play
      icon: fa-plus has-text-success
    - label: Decoupe fine et précise
      icon: fa-plus has-text-success
    - label: Multitude de materiaux possibles
      icon: fa-plus has-text-success
    - label: Vitesse de découpe
      icon: fa-minus has-text-danger
    - label: Largeur du materiau
      icon: fa-minus has-text-danger
    - label: Logiciel basique
      icon: fa-minus has-text-danger
rating: 1
todo: 5
product_toc: true
---
## Présentation :

La [Cameo 4](http://silhouettefr.fr/silhouette_cameo4.html)[^1] est une machine de découpe conçue avant tou pour les loisirs créatifs mais est également capable de découper des materiaux plus techniques comme le cuivre, et de permettre le prototypage de PCB souples. Entre autre, elle vous permet de découper avec précision le vinyle, le papier cartonné, le tissu, etc.

Les materiaux peuvent faire jusqu'à 30 centimètres de large et 3 mètres de long et la machine a la capacité de découper des images imprimées grâce à un système de détection des repères.

## Possibilités et limitations :

### a. Possibilités

### b. Limitations

## Materiaux utilisables :

## Techniques & tutos :


## Notes :

[^1]: [Silhouette](http://silhouettefr.fr/silhouette_cameo4.html)
