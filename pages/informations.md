---
layout: page
title: Informations
subtitle: fonctionnement, philsophie, accès, horaires...
menubar_toc: true
menubar: main_menu
toc_title: INDEX
show_sidebar: false
hero_image: /campus-amiens/hub-amiens/img/informations/printer.jpg
hero_darken: true
---

## C'est quoi le "MakerSpace" ?

![](/campus-amiens/hub-amiens/img/informations/drill.jpg)

Le [Makerspace](https://fr.wikipedia.org/wiki/Makerspace) est un espace de co-conception et de prototypage intégré au sein d'UniLaSalle Amiens. Il est ouvert librement aux publics du campus, à savoir les étudiants et le personnels. 

C'est un lieu de rencontre et d'apprentissage permettant à chacun de concevoir, fabriquer et partager ses projets. Il s'inscrit dans la lignée de la [culture Maker](https://fr.wikipedia.org/wiki/Culture_maker) et de la philosophie des [FabLab](https://fr.wikipedia.org/wiki/Fab_lab) dont il en est la source. A ce titre, la connaissance y est partagée et nous incitons à l'autonomie des publics dans la réalisation des projets et chacun peut travailler seul ou en groupe sur son propre sujet, qu'il soit personnel ou lié à l'activité de l'école.

Enfin, le Makerspace est un outil pédagogique fort et ancré dans l'école. Il doit permettre émergence de nouveaux usages dans la formations des étudiants et doit simplifier la mise en œuvre des projets pédagogiques. Il vient également en appui à la vie étudiante et à l'accompagnement des clubs et associations du campus. 

## Accès et horaires :

Le Makerspace est accessible librement **chaque semaine** le **jeudi** toute la journée de **9h à 12h** et de **13h à 14h**. *(Dans le cadre de projets personnels, chacun est responsable du matériel et des ressources pour sa réalisation.)*

Un atelier peut être organisé chaque semaine (perfectionnement, initiation, découverte technique, etc...).

Le reste de la semaine est dédié aux créneaux de TP et projets associés au Makerspace.

## Machines disponibles

Voici les machines : 

## Notes :

- [Definition Makerspace](https://fr.wikipedia.org/wiki/Makerspace)
- [Culture Maker - Wikipedia](https://fr.wikipedia.org/wiki/Culture_maker)
- [FabLab](https://fr.wikipedia.org/wiki/Fab_lab)