---
layout: page
title: Logiciels
subtitle: Documentation des logiciels
menubar_toc: true
menubar: main_menu
toc_title: INDEX
show_sidebar: false
hero_image: 
---

## 1. C.A.O 3D - Conception assistée par ordinateur

- [FreeCAD](../softs/freecad/freecad)

## 2. Dessin vectoriel

- [Inkscape]()

## 3. Slicer 3D

- [CURA](../softs/cura/cura)