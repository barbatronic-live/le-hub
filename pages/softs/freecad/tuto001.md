---
layout: page
title: Première pièce dans FreeCAD
subtitle: Tutoriel
menubar_toc: true
menubar: main_menu
toc_title: INDEX
show_sidebar: false
hero_image: ../img/freecad002.png
hero_darken: true
---

## 1. Objectif :

![](tuto001-001.png "Image Title"){: .shadow}

L'objectif de ce tuto est de découvrir les fonctions basiques de FreeCAD en réalisant pas-à-pas la pièce ci-dessus. L'ensemble des fonctions utilisée ne sont pas explicitées de manière exhaustives donc n'hésitez pas à tester par vous même certaines options.

### Etape 1

Dans {% include image-hover.html text="l'atelier part design" link="../tuto001/tuto001-002.png" %}, 
cliquez sur {% include image-hover.html text="créer un nouveau corps et l'activer" link="../tuto001/tuto001-003.png" %}. 
Vous devriez voir votre pièce dans le {% include image-hover.html text="TreeView à gauche" link="../tuto001/2021-08-24-14-29-50.png" %}.

{% include step-tuto.html 
content="L'atelier part design" 
link="../tuto001/tuto001-002.png" %}

{% include step-tuto.html 
content="Ceci est la **seconde** partie du tuto" 
link="../tuto001/tuto001-003.png" %}

