---
layout: page
title: Aide à la documentation
subtitle: Cheatsheet de la documentation sur le Hub
menubar_toc: true
menubar: main_menu
toc_title: INDEX
show_sidebar: false
hide_hero: true
---

# Aide à la documentation

## 1. Ressources générales

Le thème du site est basé sur le thème "[Bulma clean Theme](https://github.com/chrisrhymes/bulma-clean-theme)" modifié pour s'ajuster aux besoins du hub. Pour une documentation complète du thème, vous pouvez consulter : 
- [Demo du thème **Bulma Clean Theme**](http://www.csrhymes.com/bulma-clean-theme/)
- [Git **Bulma Clean Theme**](https://github.com/chrisrhymes/bulma-clean-theme/)

Pour la documentation liée à Bulma : [Bulma.io](http://bulma.io/documentation/)

Le processeur Markdown utilisé est **"Kramdowm"**. Vous pouvez en retrouver une documentation complète ici : [Kramdown quick ref](https://kramdown.gettalong.org/quickref.html).

## 2. Spécificités du hub

### 2.1 Images

---

#### a. Image simple
`![](image001.png "Image Title")`
![](image001.png "Image Title")

---

#### b. Image avec ombre
`![](image001.png "Image Title"){: .shadow}`
![](image001.png "Image Title"){: .shadow}

---

#### c. Image derrière un lien
`{``% include image-hover.html text="Texte" link="lien d'image" %}`
{% include image-hover.html text="Passez votre souris sur le lien" link="../aide-documentation/image001.png" %}

---

#### d. Etape de documentation

Permet de générer rapidement et facilement des étapes de documentation avec une image et un texte en markdown. Le résultat est responsive et les étapes peuvent se succéder

`{``% include step-tuto.html 
content="##### Etape 1 : 
Voici une étape de documentation. *Compatible* avec **Markdown**. " 
link="../aide-documentation/image001.png" %}`

{% include step-tuto.html 
content="##### Etape 1 : 
Voici une étape de documentation. *Compatible* avec **Markdown**. " 
link="../aide-documentation/image001.png" %}

`{``% include step-tuto.html 
content="##### Etape 1 : 
Voici la **première** étape du tutoriel." 
link="../aide-documentation/image002.png" %}`

`{``% include step-tuto.html 
content="##### Etape 2 : 
Voici la *seconde* étape du tutoriel." 
link="../aide-documentation/image003.png" %}`

**Résultat :**

{% include step-tuto.html 
content="##### Etape 1 : 
Voici la **première** étape du tutoriel." 
link="../aide-documentation/image002.png" %}

{% include step-tuto.html 
content="##### Etape 2 : 
Voici la *seconde* étape du tutoriel." 
link="../aide-documentation/image003.png" %}