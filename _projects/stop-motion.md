---
project_number: 23
hero_image: /campus-amiens/hub-amiens/img/projets/stop-motion.jpg
hero_darken: true
layout: project
image: /campus-amiens/hub-amiens/img/projets/stop-motion-4-3.jpg
product_toc: true

title: Stop motion
subtitle: Robot de prise de vue automatique
description: Création d'un robot de prise de vue automatique pour le Stop-Motion

tutor: Pierre DOUAY
tutor_mail: 'pierre.douay@unilasalle.fr'

student:
  - label: Etudiant 1
    mail: '@unilasalle.fr'
  - label: Etudiant 2
    mail: '@unilasalle.fr'
  - label: Etudiant 3
    mail: '@unilasalle.fr'

state_new: false

type: I3
area: Programmation C,Electronique,Microcontrôleur,Mécanique,Protocoles de communication
soft: Dragon frame,Arduino IDE,Fusion 360

---
## Contexte et description du projet  :

Les films d’animation en Stop Motion sont des films tournés à partir de prises de vues successives souvent effectuées à partir d’appareils photographiques. Les sujets sont déplacés manuellement entre deux séries de prises de vues. Pour augmenter et compléter le rendu de mouvements les appareils de prises de vues peuvent être mis en mouvement selon plusieurs axes. 

L’objectif de ce projet est de créer une infrastructure permettant de déplacer un appareil photographique selon plusieurs angles par rapport à la scène filmée et de pouvoir interfacer l’ensemble avec le logiciel Dragonframe un outil de la réalisation de films en stop motion. 

## Contraintes spécifiques :

Document annexe préliminaire pour les contraintes de dimensions et de mouvements (StopMotionEsiee.pdf).

## Description de l'existant :

Le précédent projet a permis de calculer les contraintes mécaniques associées à la construction de la maquette et de commander une partie des armatures ainsi que les moteurs et drivers associés. Reste à construire la maquette et lui donner vie dans un premier temps à partir d’un smartphone et ensuite à partir de Dragonframe. 

## Liens et ressources :

- [Vidéo - Motion control camera crane](https://www.youtube.com/watch?v=YF9FcSiL0Ho)
- [liens 02]()
- [liens 03]()
- [liens 04]()
- [liens 05]()
