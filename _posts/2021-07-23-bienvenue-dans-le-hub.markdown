---
layout: post
title:  "Bienvenue dans le hub"
subtitle: "Le MakerSpace arrive : retrouvez les ressources utiles en ligne !"
date: 2021-05-08 09:00:07
image: /campus-amiens/hub-amiens/img/blog-home-page.jpg
hero_image: /campus-amiens/hub-amiens/img/blog-home-page.jpg
hero_darken: true
published: true

---

Bienvenue dans le Hub du Makerspace d'UniLaSalle Amiens !

Bientôt, vous trouverez sur ce site l'ensemble des documentations réalisées en interne du hub mais également la liste des projets, les ressources nécessaires et les tutoriels sur les machines et techniques.
