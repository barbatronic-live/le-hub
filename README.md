![Build Status](https://gitlab.com/pages/jekyll/badges/master/pipeline.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

Documentation site of Unilasalle Amiens
Based on [Jekyll] website using GitLab Pages.

[Learn more about GitLab Pages](https://pages.gitlab.io) or read the the [official GitLab Pages documentation](https://docs.gitlab.com/ce/user/project/pages/).

---

# Installation
Even if this documentation page is hosted on gitlab, it is easier to use a local copy of the project to modify the website.
There is several way on running Jekyll locally depending on the platform you're using.

## Windows
You can both use Jekyll for windows of install the WSL to run the classical linux version of Jekyll
Personnaly I prefere using the WSL since it way more convenient to install additionnal gem dependecies.

---

### Get Jekyll for windows with (WSL)

<details>
<summary><b>Why WSL</b></summary>

>Th WSL is a convenient way to take advantage of Linux without setupping a dual boot on your local machine.
The WSL is a Windows10 features allowing you to run simultaneously with shared ressources and data (This is not a VM)
Using WSL with Visual studio code is a pretty convenient way to maximize your productivity if you need both Linux and Windows.

</details>

### Install Ubuntu
To get Ubuntu and Windows working together follow these steps :
1. In windows startmenu type "feature" to launch the windows feature manager [[Help]](https://docs.microsoft.com/fr-fr/windows/wsl/install-win10)
2. Select the Windows subsystem linux & click apply
3. Open Microsoft store and install Ubuntu (Restart may be required) [[Link]](https://www.microsoft.com/fr-fr/p/ubuntu/9nblggh4msv6?activetab=pivot:overviewtab)
4. Start Ubuntu and create your login and password

#### Setup Jekyll
6. Udpate package list using : 
```
sudo apt-get update -y && sudo apt-get upgrade -y
```
7. Install Ruby using : 
```
sudo apt-get install ruby-full build-essential zlib1g-dev
```
8. Change gem installation path using these command : 

```
echo '# Install Ruby Gems to ~/gems' >> ~/.bashrc
echo 'export GEM_HOME="$HOME/gems"' >> ~/.bashrc
echo 'export PATH="$HOME/gems/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc
```

* Finnaly install jekyll bundler :

```
gem install jekyll bundler
```

---

### Get Jekyll for windows without (WSL) : **Not recommended**
1. Download and run the [Ruby installer](https://rubyinstaller.org)
2. Mind to add Ruby to your ```PATH```
3. Install Jekyll and Bundler using : ```gem install jekyll bundler```
4. Check if jekyll is installed :  ```jekyll -v```

---

## Cloning project

You can clone this repo using [Git](https://git-scm.com/downloads) by typing this command :
```
git clone https://gitlab.com/makerspace-unilasalle/campus-amiens/hub-amiens.git
```
Fill in your credentials and wait for it !



## Running the website

To start the local webserver run this command from the project root: 
```
bundle exec jekyll serve
```


[Jekyll]: http://jekyllrb.com/

[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages


---
# Bulma Clean Theme

This website use the Bulma Clean theme. The theme is available as a ruby gem or can be used with GitHub pages. 

[![Gem Version](https://badge.fury.io/rb/bulma-clean-theme.svg)](https://badge.fury.io/rb/bulma-clean-theme)
![Gem](https://img.shields.io/gem/dt/bulma-clean-theme.svg)

## Ruby Gem

The ruby gem is available on the Ruby Gems website at the following location. [https://rubygems.org/gems/bulma-clean-theme](https://rubygems.org/gems/bulma-clean-theme).

## GitHub Pages

The theme can be used with GitHub Pages by setting the `remote_theme` in your Jekyll sites `_config.yml`

```yml
remote_theme: chrisrhymes/bulma-clean-theme
```

## Instructions

For full instructions, please see the Readme at the GitHub repo:
[https://github.com/chrisrhymes/bulma-clean-theme/blob/master/README.md](https://github.com/chrisrhymes/bulma-clean-theme/blob/master/README.md)

## Page Layouts

The demo site showcases the available page layout options.

* Page With Sidebar
* Page Without Sidebar
* Page With Menubar
* Page With Tabs
* Page Without Footer
* Page Without Hero
* Page With Contents
* Landing Page With Callouts
* Sponsors Page
* Image Gallery
* Recipe Page
* Blog
* Post